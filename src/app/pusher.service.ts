declare const Pusher: any;
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
 
@Injectable({
  providedIn: 'root'
})
 
export class PusherService {
  pusher: any;
  channel: any;
  constructor(private https: HttpClient) {
    this.pusher = new Pusher('5f12d528e0f7a70f97c4', {
      cluster: "ap1"
    });
    this.channel = this.pusher.subscribe('my-channel');
  }
}