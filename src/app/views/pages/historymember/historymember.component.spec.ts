import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorymemberComponent } from './historymember.component';

describe('HistorymemberComponent', () => {
  let component: HistorymemberComponent;
  let fixture: ComponentFixture<HistorymemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorymemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorymemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
