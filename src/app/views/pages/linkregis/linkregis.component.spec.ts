import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkregisComponent } from './linkregis.component';

describe('LinkregisComponent', () => {
  let component: LinkregisComponent;
  let fixture: ComponentFixture<LinkregisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkregisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkregisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
