import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarymemberComponent } from './summarymember.component';

describe('SummarymemberComponent', () => {
  let component: SummarymemberComponent;
  let fixture: ComponentFixture<SummarymemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummarymemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarymemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
