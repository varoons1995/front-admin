import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxdepositComponent } from './trxdeposit.component';

describe('TrxdepositComponent', () => {
  let component: TrxdepositComponent;
  let fixture: ComponentFixture<TrxdepositComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxdepositComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxdepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
