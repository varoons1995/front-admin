import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxhiddenComponent } from './trxhidden.component';

describe('TrxhiddenComponent', () => {
  let component: TrxhiddenComponent;
  let fixture: ComponentFixture<TrxhiddenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxhiddenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxhiddenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
