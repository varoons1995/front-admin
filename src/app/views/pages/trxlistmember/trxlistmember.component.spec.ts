import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxlistmemberComponent } from './trxlistmember.component';

describe('TrxlistmemberComponent', () => {
  let component: TrxlistmemberComponent;
  let fixture: ComponentFixture<TrxlistmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxlistmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxlistmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
