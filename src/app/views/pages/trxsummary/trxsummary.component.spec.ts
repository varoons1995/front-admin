import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxsummaryComponent } from './trxsummary.component';

describe('TrxsummaryComponent', () => {
  let component: TrxsummaryComponent;
  let fixture: ComponentFixture<TrxsummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxsummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxsummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
