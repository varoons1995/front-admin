import { Component, OnInit ,ChangeDetectorRef} from '@angular/core';
import { FormControl } from '@angular/forms'
import axios from 'axios';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';


@Component({
  selector: 'kt-trxsummary',
  templateUrl: './trxsummary.component.html',
  styleUrls: ['./trxsummary.component.scss']
})
export class TrxsummaryComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  displayedColumnsTableffirst: any[] = ['DEPOSIT', 'WITHDRAW'];
  dataSourceTablefirst ;

  displayedColumnsTablesecond: any[] = ['SUM_TRANSACTION_DEPOSIT', 'SUM_TRANSACTION_WITHDAW' , 'NET_PROFIT'];
  dataSourceTablesecond ;
  // datepicker
  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  websiteAll;
  agentType = "";
  username=""
  trx;


  // api
  api = environment.apibackend;
  constructor(private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>) { }

  ngOnInit(): void {
    this.onSearch();
    this.changeDetectorRefs.detectChanges();
  }

  onSearch(){
    let get_frist_date = this.creatDate.value;
    let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
    let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
    let frist_year = get_frist_date.getFullYear();
    let f_date = frist_year + "-" + frist_month + "-" + frist_date;
  
    let get_last_date = this.endDate.value;
    let last_date = ("0" + get_last_date.getDate()).slice(-2);
    let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
    let last_year = get_last_date.getFullYear();
    let l_date = last_year + "-" + last_month + "-" + last_date;
    axios({
      method: 'post',
      url: this.api+'/credit/sumcredit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data :{
        "CREATEDATE":f_date,
        "ENDDATE":l_date
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
      console.log(response.data)
      this.trx = response.data
      this.changeDetectorRefs.detectChanges();
      console.log(this.trx)
        }
      })
      .catch(err => {
      console.error(err)
      this.store.dispatch(new Logout());
      })   
    }
}
